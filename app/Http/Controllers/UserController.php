<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\UserServices;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(
         private readonly UserServices $userServices
    )
    {
    }

    public function userList()
    {
        return $this->userServices->index();
    }

    /**
     * @param Request $request
    */
    public function loginUser(Request $request)
    {
        return $this->userServices->login($request);
    }

    public function logoutUser()
    {
        return $this->userServices->logout();
    }

    public function createUser(Request $request)
    {
        return $this->userServices->create(($request));
    }

    /**
     * @param Request $request
     * @param User $user
    */
    public function updateUser(Request $request, User $user)
    {
        return $this->userServices->update($request, $user);
    }

    public function deleteUser(User $user)
    {
        return $this->userServices->destroy($user);
    }
}
