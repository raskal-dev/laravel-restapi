<?php

namespace App\Services;

use App\Http\Controllers\BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Laravel\Passport\TokenRepository;
use Mockery\Exception;

class UserServices extends BaseController
{
    public function login($request)
    {
        $user = User::where('email', $request->email)->first();
        // if(!$user) return response(["message"=> "User not found"], 404);
        if(!$user) return $this->sendResponse([],"User not found", 404 );

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $token = $user->createToken('user_token')->accessToken;
            return $this->sendResponse($token, "User logged successfully !");
        }
        return $this->sendResponse([], "Password incorrect.", 401);
    }

    public function logout()
    {
        $tokens = Auth::user()->tokens;
        $tokenRepository = app(TokenRepository::class);
        foreach ($tokens as $token) {
            $tokenRepository->revokeAccessToken($token->id);
        }
        return $this->sendResponse([], 'User logged out');
    }

    public function index()
    {
        return $this->sendResponse(User::all());
    }


    public function create($request)
    {
        try {
            User::create([
                "name" => $request->name,
                "email" => $request->email,
                "password" => bcrypt($request->password),
            ]);
            return response(["message" => "User created!"]);
        } catch (Exception $e) {
            return response(["error" => $e]);
        }
    }

    public function update($request, $user)
    {
        try{
            User::find($user->id)->update($request);
            return response(["message"=>"User updated!"]);
        }
        catch (Exception $e){
            return response(["error"=>$e]);
        }
    }

    public function destroy($user)
    {
        try{
            User::find($user->id)->delete();
            return response(["message"=>"User deleted!"]);
        }
        catch (Exception $e){
            return response(["error"=>$e]);
        }
    }

}
