<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [\App\Http\Controllers\UserController::class, 'loginUser']);

Route::prefix('/users')->middleware('auth:api')->group(function(){
    Route::post('/', [\App\Http\Controllers\UserController::class, 'createUser']);
    Route::get('/', [\App\Http\Controllers\UserController::class, 'userList']);
    Route::get('/logout', [\App\Http\Controllers\UserController::class, 'logoutUser']);
});

Route::get('/test', function(){
    return response(['user_connected'=> auth('api')->user()->tokens]);
})->middleware('auth:api');
